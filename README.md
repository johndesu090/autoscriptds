Premium AutoScript Debian Stretch

VPN AutoScript is made by FordSenpai to minimize the time consumed and user involvement in setting up your VPS


[Donations] GCASH: 09206200840 PAYPAL: johnford090@gmail.com FACEBOOK: John Ford Mangiliman

Facebook Support Link: https://www.facebook.com/johndesu090

Supported Linux Distribution: Debian9

[Services]


OpenSSH

Webmin

BadVPN

Dropbear

Stunnel

OpenVPN

Squid3

Privoxy

Adblocker

Malware Blocker

ShadowsocksR (Optional)




Installation (Default Version)

  ```
wget https://bitbucket.org/johndesu090/autoscriptds/raw/51baf51344c2a6c1f21574a982dc7d23b06d016c/bDS && chmod +x bDS && ./bDS
  ```
  
Installation (Single Cert Apache)

  ```
wget https://bitbucket.org/johndesu090/autoscriptds/raw/51baf51344c2a6c1f21574a982dc7d23b06d016c/bDSCA && chmod +x bDSCA && ./bDSCA
  ```
 
Installation (Single Cert Nginx)

  ```
wget https://bitbucket.org/johndesu090/autoscriptds/raw/51baf51344c2a6c1f21574a982dc7d23b06d016c/bDSCAN && chmod +x bDSCAN && ./bDSCAN
  ```
  
Installation (Single Cert w0pw0p)

  ```
wget https://bitbucket.org/johndesu090/autoscriptds/raw/51baf51344c2a6c1f21574a982dc7d23b06d016c/bDSWOP && chmod +x bDSWOP && ./bDSWOP
  ```


Installation OpenVPN-Monitor

  ```
wget https://bitbucket.org/johndesu090/autoscriptds/raw/51baf51344c2a6c1f21574a982dc7d23b06d016c/Files/Plugins/ovpnmonitor.sh && chmod +x ovpnmonitor.sh && ./ovpnmonitor.sh
  ```


Additional Info
Recommended OS: Debian 9 Stretch x64

ShadowsocksR (Optional)

   Download (Android): https://github.com/shadowsocksr-backup/shadowsocksr-android/releases
  
   Download (Windows): https://github.com/shadowsocksr-backup/shadowsocksr-csharp/releases
  
   Configuration file path: /etc/shadowsocks.json 
   
   Log file path: /var/log/shadowsocks.log 
   
   Installation directory: /usr/local/shadowsocks
   
   
   Installation:
   
```
wget --no-check-certificate https://bitbucket.org/johndesu090/autoscriptds/raw/51baf51344c2a6c1f21574a982dc7d23b06d016c/Files/Plugins/SSR
chmod +x SSR
./SSR 2>&1 | tee shadowsocksR.log
```
   Commands:
```
Start: /etc/init.d/shadowsocks start 
Stop: /etc/init.d/shadowsocks stop 
Restart: /etc/init.d/shadowsocks restart 
Status: /etc/init.d/shadowsocks status
```

   Multi Users; Config Setup:
```
{
"server":"0.0.0.0",
"server_ipv6": "[::]",
"local_address":"127.0.0.1",
"local_port":1080,
"port_password":{
    "8989":"password1",
    "8990":"password2",
    "8991":"password3"
},
"timeout":300,
"method":"aes-256-cfb",
"protocol": "origin",
"protocol_param": "",
"obfs": "plain",
"obfs_param": "",
"redirect": "",
"dns_ipv6": false,
"fast_open": false,
"workers": 1
}
```


